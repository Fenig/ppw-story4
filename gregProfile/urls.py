from django.urls import path
from .views import profile, experience, register_get, register_post, validate, webservice_get_subscriber, webservice_delete_subscriber
#url for app
urlpatterns = [
    path('', profile, name='default'),
    path('profile/', profile, name='profile_page'),
    path('register_page/', register_get, name='register_page'),
    path('register/', register_post, name='register'),
    path('experience/', experience, name='experience_page'),
    path('validate-register/', validate, name='validate'),
    path('get_subscriber/', webservice_get_subscriber, name='get_subscriber'),
    path('delete_subscriber/', webservice_delete_subscriber, name='delete_subscriber'),
]