from django import forms
from .models import CustomUsers

class RegisterForm(forms.ModelForm):
    class Meta:
        model = CustomUsers
        fields = ('name', 'email', 'password')
        widgets = {
            'name' : forms.TextInput(attrs={'class' : 'form-control form-control-sm input-custom mt-1 mb-1', 'placeholder' : 'name'}),
            'email' : forms.TextInput(attrs={'class' : 'form-control form-control-sm input-custom mt-1 mb-1', 'type' : 'email', 'placeholder' : 'email'}),
            'password' : forms.TextInput(attrs={'class' : 'form-control form-control-sm input-custom mt-1 mb-1', 'type' : 'password', 'placeholder' : 'password'}),
        }




