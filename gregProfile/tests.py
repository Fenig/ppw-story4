from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
# Create your tests here.

from .views import profile, register_get, register_post, experience, validate

class UnitTest(TestCase):
    def test_profile_using_correct_template(self):
        response = Client().get(reverse('profile_page'))
        self.assertTemplateUsed(response, 'profile.html')

    def test_register_using_correct_template(self):
        response = Client().get(reverse('register_page'))
        self.assertTemplateUsed(response, 'register.html')
    
    def test_experience_using_correct_template(self):
        response = Client().get(reverse('experience_page'))
        self.assertTemplateUsed(response, 'experience.html')

    def test_profile_url_is_exist(self):
        response = Client().get(reverse('profile_page'))
        self.assertEqual(response.status_code,200)
    
    def test_register_get_url_is_exist(self):
        response = Client().get(reverse('register_page'))
        self.assertEqual(response.status_code,200)
    
    def test_register_post_url_is_exist(self):
        response = Client().post(reverse('register'))
        self.assertEqual(response.status_code,200)
    
    def test_experience_url_is_exist(self):
        response = Client().get(reverse('experience_page'))
        self.assertEqual(response.status_code,200)
    
    def test_validate_register_url_is_exist(self):
        response = Client().get(reverse('validate'))
        self.assertEqual(response.status_code,200)


    def test_using_profile_func(self):
        found = resolve('/')
        self.assertEqual(found.func, profile)
    
    def test_using_register_get_func(self):
        found = resolve('/register_page/')
        self.assertEqual(found.func, register_get)
    
    def test_using_register_post_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register_post)
    
    def test_using_experience_func(self):
        found = resolve('/experience/')
        self.assertEqual(found.func, experience)
    
    def test_using_validate_func(self):
        found = resolve('/validate-register/')
        self.assertEqual(found.func, validate)
