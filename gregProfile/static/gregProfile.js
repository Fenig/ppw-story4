function del(idx) {
    $.ajax({
        url: '/delete_subscriber/',
        method: 'GET',
        headers: {
            'X-CSRF-Token': $("input[name=csrfmiddlewaretoken]").val(),
        },
        data: {
            'email':$("#"+idx+'P').text(),
            'password':$("#"+idx+'I').val(),
        },
        dataType: 'json',
        success: function (data) {
            renderSubscriber();
            alert(data.response);
        }
    });
}

function renderSubscriber() {
    $.ajax({
        url: '/get_subscriber/',
        headers: {
            'X-CSRF-Token': $("input[name=csrfmiddlewaretoken]").val(),
        },
        success: function (data) {
            var res = "";
            var sub = data.subscribers;
            
            for (var i=0; i<sub.length; i+=1) {
                res += `
                <li>
                    <div class="row">
                    <b class="col-md-3">`+ sub[i][0] +`</b>
                    <p class="col-md-4" id="btn`+i+`P">`+ sub[i][1] +`</p>
                    <button onclick="del('btn`+i+`')" class="col-md1 m-1 btn" id="btn`+i+`">delete</button>
                    <input type="password"placeholder="password" class="col-md-3" id="btn`+i+`I">
                    </div>
                </li> `
            }
            $("#list-subscribers").html(res);
        }
    });
}

$(() => {
    $("#submit-btn").hide();

    $("#sub-trigger").click(() => {
        renderSubscriber();
    });

    $("#id_email").change(() => {
        $.ajax({
            url: '/validate-register',
            headers: {
                'X-CSRF-Token': $("input[name=csrfmiddlewaretoken]").val(),
            },
            data: {
              'email': $("#id_email").val(),
            },
            dataType: 'json',
            success: function (data) {
                
                if (data.is_taken) {
                    $('#results').html(`<div class='alert-box alert radius' data-alert>Email is not available!
                    <a href='#' class='close'>&times;</a></div>`);
                    $("#submit-btn").hide();
                } else {
                    $('#results').html("");
                    $("#submit-btn").show();
                }
            }
        });
    });

    $("#submit-btn").click((e) => {
        e.preventDefault();
        $.ajax({
            url : "/register/", 
            type : "POST",
            headers: {
                'X-CSRF-Token': $("input[name=csrfmiddlewaretoken]").val()
            },
            data : $("form").serialize(), 
            dataType: 'json',
            success : function(resp) {
                alert("yay");
                $('#id_password').val('');
                $('#id_name').val('');
                $('#id_email').val('');
                alert(resp.result);
                console.log(resp);
                console.log("success");
            },
            error : function(resp){
                alert (resp.result);
                console.log(resp);
            }
        });
    });


})


