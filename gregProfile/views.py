from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import render
from .models import CustomUsers
from .forms import RegisterForm
import json
def profile(request):
    return render(request, 'profile.html', {})

def register_post(request):
    if request.method == 'POST':
        try:
            name = request.POST.get('name')
            email = request.POST.get('email')
            password = request.POST.get('password')
            
            
            if(len(name) < 51 and len(email) < 51 and len(password) < 51):
                CustomUsers.objects.create(name=name, email=email, password=password)
                return JsonResponse({"result" : "Success, You are a subscriber!"}, safe=False)
            return JsonResponse({"result" : "Data entered not valid!"}, safe=False)
        except:
            return JsonResponse({"result" : "Failed, something bad happened."}, safe=False)

def register_get(request):
    form = RegisterForm()
    return render(request, 'register.html', {'form': form})

def experience(request):
    return render(request, 'experience.html', {})

def validate(request):
    email = request.GET.get('email')

    is_taken = True

    obj = CustomUsers.objects.filter(email=email).first()

    if obj == None: is_taken = False

    json = JsonResponse( {"is_taken":is_taken}, safe=False )
    return json

def webservice_get_subscriber(request):
    subscribers = CustomUsers.objects.all()
    result = {}
    tmp = []
    for i in subscribers:
         tmp.append([i.name, i.email])
    result["subscribers"] = tmp
    return JsonResponse(result, safe=False)

def webservice_delete_subscriber(request):
    email = request.GET.get('email')
    passwd = request.GET.get('password')

    obj = CustomUsers.objects.filter(email=email).first()
    if obj == None: return JsonResponse({"response":"Something went wrong"})

    if passwd=="" or obj.password != passwd: return JsonResponse({"response":"Password wrong"})

    obj.delete()
    return JsonResponse({"response":"deleted!"})
